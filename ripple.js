var Ripple = {
    init: function(selector){
        document.querySelectorAll(selector).forEach(el => this.transform(el))
    }, 
    transform:function(elemento){
        console.log(elemento);
        elemento.addEventListener('click', function(ev){
            elemento.style.position = 'relative';
            let coords= { 
                    x: ev.x,
                    y: ev.y  
            }

            let rect = elemento.getBoundingClientRect();
            let clickPosition = {
                x:coords.x - rect.left,
                y:coords.y - rect.top
            }
            let ripple = document.createElement('span');
            let style = `
                            
                            width:${rect.width * 2}px; 
                            height:${rect.width * 2}px; 
                            background-color:white;
                            opasity:0.56;
                            border-radius:50%;
                            position:absolute;                            
                            top: ${clickPosition.y - rect.width}px;
                            left: ${clickPosition.y- rect.width}px;
                            transform:scale(0);
                            transition: all 0.3s cubic-bezier(0.3,0.0,0.2,1);
                        `;
            ripple.style.cssText = style;

            setTimeout(()=>{
                ripple.style.transform = 'scale(1)';
                ripple.style.opacity= '0';
            },50);
        
            setTimeout(()=>{
                ripple.remove();
            },350);
            elemento.appendChild(ripple);

            console.log(rect);
            console.log(coords);
        });
        
    }
}